(function(){
	var categoria = $('.current-category').html();
	var cat = categoria.replace('Currently Browsing: ','').replace(/\s+/g, ' ');
	var data = [];
	$('body').append('<div class="myTexts"></div>');
	$('.home-post-wrap2').each(function(){
		var titolo = $(this).find('.titles2').find('a');
		var thumb = $(this).find('.thumbnail-div-3').find('img');
		var testo = $(this).find('.single-entry').text();

		var myData = {
			categoria: cat,
			titolo: titolo.text().replace(/\s+/g, ' '),
			url: titolo.attr('href'),
			thumb: thumb.attr('src'),
			thumbAlt: thumb.attr('alt'),
			shortText: testo
		};
		data.push(myData);
		$('body').find('.myTexts').append('<textarea class="myText" style="clear:both;">'+JSON.stringify(myData).replace(/\\t+/g,'').replace(/\\n+/g,'')+'</textarea>');
		
	});
	
	
	
})();

(function(){
	$('body').append('<div class="myT"></div>');
	$('textarea').each(function(){
		$('body').find('.myT').append($(this).text());
	});
})();
